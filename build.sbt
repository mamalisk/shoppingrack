name := "myshoppingrack"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache
)     

libraryDependencies += ("ws.securesocial" %% "securesocial" % "2.1.3").exclude("play", "*")

libraryDependencies += ("org.twitter4j"% "twitter4j-core"% "3.0.3").exclude("play", "*")

libraryDependencies += "net.sourceforge.htmlunit" % "htmlunit" % "2.14"

libraryDependencies += ("org.reactivemongo" %% "play2-reactivemongo" % "0.10.0").exclude("play", "*")

libraryDependencies += ("com.sksamuel.scrimage" %% "scrimage-core" % "1.4.1").exclude("play", "*")

libraryDependencies += ("com.sksamuel.scrimage" %% "scrimage-canvas" % "1.4.1").exclude("play", "*")

libraryDependencies += ("com.sksamuel.scrimage" %% "scrimage-filters" % "1.4.1").exclude("play", "*")

libraryDependencies += ("nl.rhinofly" %% "api-s3" % "3.1.0").exclude("play", "*")

libraryDependencies += "commons-io" % "commons-io" % "2.4"

conflictWarning := ConflictWarning.disable

resolvers += "Rhinofly Internal Repository" at "http://maven-repository.rhinofly.net:8081/artifactory/libs-release-local"

play.Project.playScalaSettings
