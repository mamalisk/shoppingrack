package models

package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Avatar( username:String,
                   creationDate: Option[DateTime],
                   updateDate: Option[DateTime]
                    )

object Avatar {
  implicit object AvatarBSONReader extends BSONDocumentReader[Avatar] {
    def read(doc:BSONDocument) : Avatar =
      Avatar(
        doc.getAs[String]("username").get,
        doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
        doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
      )
  }

  implicit object AvatarBSONWriter extends BSONDocumentWriter[Avatar] {
    def write(avatar:Avatar) : BSONDocument =
      BSONDocument(
        "username" -> avatar.username,
        "creationDate" -> avatar.creationDate.map(date => BSONDateTime(date.getMillis)),
        "updateDate" -> avatar.updateDate.map(date => BSONDateTime(date.getMillis))
      )
  }

}

