package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._
import controllers.ProfileService


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Profile(username:String,
                   userId:String,
                   providerId:String,
                   avatar: Option[String],
                   description: Option[String],
                   location: Option[String],
                   creationDate: Option[DateTime],
                   updateDate: Option[DateTime]
                    )

case class PartialProfile(username:String, avatar:Option[String], description: Option[String], location: Option[String])


object Profile {
  implicit object ProfileBSONReader extends BSONDocumentReader[Profile] {
    def read(doc:BSONDocument) : Profile =
        Profile(
           doc.getAs[String]("username").get,
           doc.getAs[String]("userId").get,
           doc.getAs[String]("providerId").get,
           doc.getAs[String]("avatar"),
           doc.getAs[String]("description"),
           doc.getAs[String]("location"),
           doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
           doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
        )
  }

  implicit object ProfileBSONWriter extends BSONDocumentWriter[Profile] {
    def write(profile:Profile) : BSONDocument =
       BSONDocument(
          "username" -> profile.username,
          "userId"   -> profile.userId,
          "providerId" -> profile.providerId,
          "avatar"  -> profile.avatar,
          "description" -> profile.description,
          "location" -> profile.location,
          "creationDate" -> profile.creationDate.map(date => BSONDateTime(date.getMillis)),
          "updateDate" -> profile.updateDate.map(date => BSONDateTime(date.getMillis))
       )
  }

  val form = Form(
    mapping(
      "username" -> nonEmptyText.verifying("This username already exists", ProfileService.doesNotExist(_)),
      "avatar" -> optional(text),
      "description" -> optional(text),
      "location" -> optional(text)
      ) { (username, avatar, description, location) =>
      PartialProfile(
        username,
        avatar,
        description,
        location)
    } { profile =>
      Some(
        (
          profile.username,
          profile.avatar,
          profile.description,
          profile.location
          ))
    })

}
