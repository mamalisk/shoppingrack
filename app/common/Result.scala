package common

/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Result(success:Boolean, errors:List[String])
