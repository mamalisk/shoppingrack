package controllers


import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial


import play.api.Play._

import fly.play.s3._
import play.api.libs.concurrent._
import play.api.libs.concurrent.Execution.Implicits._
import org.apache.commons.io._
import models.Profile
import scala.concurrent.Future
import org.joda.time.DateTime

/**
 * Created by kostasmamalis on 08/07/2014.
 */
object Profiles extends Controller with SecureSocial {

  val bucketName = Play.application.configuration.getString("my.s3bucket").get
  val bucket = S3(bucketName)
  val bucketDir = "avatars/"


  def index = SecuredAction.async { implicit request =>
    val result = bucket.list(bucketDir)
    result.map( list => Ok(views.html.index(list,"your contents")))
  }

  def upload = SecuredAction(parse.multipartFormData) { implicit request =>
    request.body.file("file").map { file =>
      val result = bucket + BucketFile(bucketDir + file.filename, file.contentType.get, FileUtils.readFileToByteArray(file.ref.file))
      val all = bucket.list(bucketDir)
      all.map( list => Ok(views.html.index(list,"your contents")))
    }
    val all = bucket.list(bucketDir)
    Redirect(routes.Profiles.index)
  }

  def create = SecuredAction.async { implicit request=>
    if(ProfileService.existsAlready(request.user.identityId)) InternalServerError("user already exists")
      Profile.form.bindFromRequest.fold(
        errors => Future.successful(Ok(views.html.profiles.create(errors))),
        profile => {
          ProfileService.create(Profile(
            profile.username,
            request.user.identityId.userId,
            request.user.identityId.providerId,
            profile.avatar,
            profile.description,
            profile.location,
            Some(new DateTime()),
            Some(new DateTime())
            )
          )
          Future.successful(Ok(views.html.index(None,"created")))
        }
      )
  }

  def edit = SecuredAction {
    Ok(views.html.profiles.create(Profile.form))
  }
}
