package controllers

import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial
import play.api.libs.json._

object Application extends Controller with SecureSocial {

  def index = SecuredAction {
    Ok(views.html.index(List(),"Your new application is ready."))
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(Routes.javascriptRouter("jsRoutes")(
      routes.javascript.Application.previousRecordings,
      routes.javascript.Application.search
    )).as("text/javascript")
  }

  def previousRecordings() = SecuredAction(ajaxCall=true) { implicit secureRequest =>
    println("previousRecordings handler responded")
    Ok
  }

  def search(input: String) = SecuredAction(ajaxCall=true) { implicit request =>
    val result = null
    Ok(Json.obj("result" -> JsString("result")))
  }

  def demo = SecuredAction {
    implicit request =>
      Ok(views.html.demo())
  }

}